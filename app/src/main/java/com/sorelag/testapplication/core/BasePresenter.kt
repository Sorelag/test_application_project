package com.sorelag.testapplication.core

import java.lang.ref.Reference
import java.lang.ref.WeakReference

abstract class BasePresenter<V : BasePresentationContract.BaseView> :
    BasePresentationContract.BasePresenter<V> {

    private var mView: Reference<V>? = null

    protected val view: V?
        get() = mView!!.get()

    override fun attachView(view: V) {
        this.mView = WeakReference(view)
    }

    override fun detachView() {
        this.mView!!.clear()
    }
}
