package com.sorelag.testapplication.core

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.sorelag.testapplication.application.MainApplication
import com.sorelag.testapplication.di.component.AppComponent

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        extractIntentData(intent)
        setupDependencies((application as MainApplication).getAppComponent())
    }

    protected fun extractIntentData(intent: Intent) {}


    fun getRootView(): View {
        return findViewById<View>(android.R.id.content)
    }

    protected abstract fun setupDependencies(appComponent: AppComponent)
}