package com.sorelag.testapplication.application.utils

import android.content.Context

class NetworkConnectionWrapper(val mContext: Context)  {

    fun isNetworkAvailable(): Boolean {
        return ConnectionUtils().isNetworkAvailable(mContext)
    }
}