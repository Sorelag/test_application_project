package com.sorelag.stestapplication.application

import android.app.Activity
import android.app.Application
import android.os.Bundle
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

class AppActivityManager : Application.ActivityLifecycleCallbacks {

    lateinit var mRegistered : AtomicBoolean
    lateinit var mActivitiesRef : HashMap<String, WeakReference<Activity>>

    @Inject
    fun AppActivityManager(){
        mRegistered = AtomicBoolean(false)
    }

    fun register(application: Application) {
        if (!mRegistered.get()) {
            application.registerActivityLifecycleCallbacks(this)
            mRegistered.set(true)
        }
    }

    fun getAliveActivity(): Activity? {
        var activity: Activity? = null
        for (reference in mActivitiesRef.values) {
            if (!reference.isEnqueued) {
                activity = reference.get()
                break
            }
        }
        return activity
    }

    override fun onActivityPaused(p0: Activity?) {
        // empty
    }

    override fun onActivityResumed(p0: Activity?) {
        // empty
    }

    override fun onActivityStarted(p0: Activity?) {
        // empty
    }

    override fun onActivityDestroyed(p0: Activity?) {
        mActivitiesRef.remove(p0!!::class.java.name)
    }

    override fun onActivitySaveInstanceState(p0: Activity?, p1: Bundle?) {
        // empty
    }

    override fun onActivityStopped(p0: Activity?) {
        // empty
    }

    override fun onActivityCreated(p0: Activity?, p1: Bundle?) {
        // empty
    }
}