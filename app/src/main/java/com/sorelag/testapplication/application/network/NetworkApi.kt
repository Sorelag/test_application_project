package com.sorelag.testapplication.application.network

import com.sorelag.testapplication.data.entities.Rates
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkApi {

    @GET("latest?")
    fun getData(@Query("base") value: String): Observable<Rates>
}