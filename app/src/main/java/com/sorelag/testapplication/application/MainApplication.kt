package com.sorelag.testapplication.application

import android.app.Activity
import android.app.Application
import android.app.Service
import androidx.appcompat.app.AppCompatDelegate
import com.facebook.stetho.Stetho
import com.sorelag.testapplication.BuildConfig
import com.sorelag.testapplication.di.CoreModule
import com.sorelag.testapplication.di.DaggerCoreComponent
import com.sorelag.testapplication.di.component.AppComponent
import com.sorelag.testapplication.di.component.DaggerAppComponent
import com.sorelag.testapplication.di.module.AppModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import timber.log.Timber
import javax.inject.Inject

class MainApplication : Application(), HasActivityInjector, HasServiceInjector {

    @Inject
    lateinit var mActivityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var mServiceInjector: DispatchingAndroidInjector<Service>

    private var mAppComponent: AppComponent? = null

    fun getAppComponent(): AppComponent {
        checkNotNull(mAppComponent)
        return mAppComponent as AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        injectDependency()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
        }

        mAppComponent!!.inject(this)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    fun injectDependency() {
        mAppComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .coreComponent(
                DaggerCoreComponent.builder()
                    .coreModule(CoreModule(this))
                    .build()
            )
            .build()
    }

    override fun activityInjector(): DispatchingAndroidInjector<Activity>? {
        return mActivityInjector
    }

    override fun serviceInjector(): AndroidInjector<Service>? {
        return mServiceInjector
    }

}
