package com.sorelag.testapplication.di

import android.content.Context
import com.sorelag.testapplication.application.network.NetworkApi
import com.sorelag.testapplication.application.utils.NetworkConnectionWrapper
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(CoreModule::class))
interface CoreComponent {

    fun provideContext(): Context

    fun provideApi(): NetworkApi

    fun networkConnectionWrapper(): NetworkConnectionWrapper

    @Component.Builder
    interface Builder {

        fun coreModule(coreModule: CoreModule): Builder

        fun build(): CoreComponent
    }
}