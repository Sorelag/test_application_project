package com.sorelag.testapplication.di

import com.sorelag.testapplication.application.network.NetworkApi
import com.sorelag.testapplication.data.repository.MainRepository
import com.sorelag.testapplication.data.repository.MainRepositoryImpl
import com.sorelag.testapplication.di.scopes.FeatureScope
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
object RepositoryModule {

    @FeatureScope
    @JvmStatic
    @Provides
    internal fun provideRepository(
        api: NetworkApi,
        @Named(RxModule.NETWORK) networkScheduler: Scheduler
    ): MainRepository {
        return MainRepositoryImpl(api, networkScheduler)
    }
}