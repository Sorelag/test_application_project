package com.sorelag.testapplication.di

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executors
import javax.inject.Named

@Module
object RxModule {

    const val NETWORK = "network"
    const val MAIN = "main"
    const val COMPUTATION = "computation"

    @JvmStatic
    @Provides
    @Named(COMPUTATION)
    fun providerComputationScheduler(): Scheduler {
        return Schedulers.computation()
    }

    @JvmStatic
    @Provides
    @Named(NETWORK)
    fun providerNetworkScheduler(): Scheduler {
        val threads = Math.max(Runtime.getRuntime().availableProcessors() / 2, 1)
        return Schedulers.from(Executors.newFixedThreadPool(threads))
    }

    @JvmStatic
    @Provides
    @Named(MAIN)
    fun providerMainScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}