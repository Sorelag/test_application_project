package com.sorelag.testapplication.di.scopes

import javax.inject.Scope

@Retention(AnnotationRetention.SOURCE)
@Scope
annotation class FeatureScope