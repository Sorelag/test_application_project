package com.sorelag.testapplication.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [RxModule::class,
        ApiModule::class]
)
class CoreModule(private val mApplication: Application) {

    @Singleton
    @Provides
    fun provideApplication(): Application {
        return mApplication
    }

    @Singleton
    @Provides
    internal fun provideContext(): Context {
        return mApplication
    }
}