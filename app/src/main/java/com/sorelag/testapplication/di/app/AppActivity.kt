package com.sorelag.testapplication.di.app

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.sorelag.testapplication.application.MainApplication
import com.sorelag.testapplication.di.component.AppComponent

abstract class AppActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        extractIntentData(intent)
        setupDependencies((application as MainApplication).getAppComponent())
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    fun initActionBar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

    protected fun extractIntentData(intent: Intent) {}

    /**
     * The method tells us to setup needed dependencies by injecting specific graphs.
     *
     *
     * Note: Dagger requires us to inject dependencies into the specific destinations, not a 'base' implementations
     *
     * @param appComponent app-wide dependencies graph
     */
    protected abstract fun setupDependencies(appComponent: AppComponent)

}
