package com.sorelag.testapplication.di.module

import android.app.Activity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private var mActivity: Activity) {

    @Provides
    fun provideActivity(): Activity {
        return mActivity
    }
}