package com.sorelag.testapplication.di.scopes

import javax.inject.Scope

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.TYPE)
@Retention(AnnotationRetention.SOURCE)
@Scope
annotation class FragmentScope