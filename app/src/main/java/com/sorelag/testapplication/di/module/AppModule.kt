package com.sorelag.testapplication.di.module

import android.app.Application
import com.sorelag.testapplication.di.RepositoryModule
import com.sorelag.testapplication.di.RxModule
import com.sorelag.testapplication.di.scopes.FeatureScope
import dagger.Module
import dagger.Provides

@Module(
    includes = [RepositoryModule::class,
        RxModule::class]
)
class AppModule(private val mApplication: Application) {

    @FeatureScope
    @Provides
    fun provideApplication(): Application {
        return mApplication
    }
}