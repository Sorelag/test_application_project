package com.sorelag.testapplication.di.module.main

import com.sorelag.testapplication.di.scopes.ActivityScope
import com.sorelag.testapplication.ui.MainActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = arrayOf(MainActivityModule::class))
interface MainActivityComponent {

    fun inject(activity: MainActivity)

    @Subcomponent.Builder
    interface Builder {

        fun module(module: MainActivityModule): Builder

        fun build(): MainActivityComponent
    }
}