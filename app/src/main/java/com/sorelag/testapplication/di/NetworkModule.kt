package com.sorelag.testapplication.di

import android.app.Application
import android.content.Context
import androidx.annotation.NonNull
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.sorelag.testapplication.BuildConfig
import com.sorelag.testapplication.application.utils.BasicInterceptor
import com.sorelag.testapplication.application.utils.NetworkConnectionWrapper
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
object NetworkModule {

    const val SIMPLE = "simple"

    private const val CACHE_SIZE = (10 * 1024).toLong()
    private const val TIMEOUT: Long = 60
    private const val CACHE_DIR = "network"
    private const val NAME_BASE_URL = "NAME_BASE_URL"


    @Provides
    @JvmStatic
    @Singleton
    @Named(NAME_BASE_URL)
    internal fun providesBaseUrl(): String {
        return BuildConfig.BASE_API_URL
    }

    @Provides
    @JvmStatic
    @Singleton
    @Named(SIMPLE)
    internal fun provideRetrofitAdapter(
        @NonNull @Named(NAME_BASE_URL) baseUrl: String,
        @NonNull @Named(SIMPLE) httpClient: OkHttpClient,
        @NonNull mapper: ObjectMapper
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(JacksonConverterFactory.create(mapper))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient)
            .build()
    }

    @Singleton
    @JvmStatic
    @Provides
    internal fun provideObjectMapper(): ObjectMapper {
        val mapper = ObjectMapper()
        mapper.registerModule(KotlinModule())
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        return mapper
    }

    @Singleton
    @JvmStatic
    @Provides
    internal fun provideCache(@NonNull context: Context): Cache {
        val cacheDir = File(context.cacheDir, CACHE_DIR)
        return Cache(cacheDir, CACHE_SIZE)
    }

    @Named(SIMPLE)
    @Singleton
    @JvmStatic
    @Provides
    internal fun provideOkHttpClient(
        @NonNull cache: Cache,
        @NonNull interceptor: Interceptor
    ): OkHttpClient {
        return OkHttpClient().newBuilder()
            .cache(cache)
            .addInterceptor(interceptor)
            .addNetworkInterceptor(StethoInterceptor())
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .build()

    }

    @JvmStatic
    @Provides
    internal fun networkConnectionWrapper(application: Application): NetworkConnectionWrapper {
        return NetworkConnectionWrapper(application)
    }

    @JvmStatic
    @Singleton
    @Provides
    internal fun provideInterceptor(): Interceptor {
        return BasicInterceptor()
    }
}