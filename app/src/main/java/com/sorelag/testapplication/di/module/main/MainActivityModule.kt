package com.sorelag.testapplication.di.module.main

import com.sorelag.testapplication.application.utils.NetworkConnectionWrapper
import com.sorelag.testapplication.data.repository.MainRepository
import com.sorelag.testapplication.di.RxModule
import com.sorelag.testapplication.di.scopes.ActivityScope
import com.sorelag.testapplication.models.MainModel
import com.sorelag.testapplication.models.MainModelImpl
import com.sorelag.testapplication.ui.MainActivity
import com.sorelag.testapplication.ui.MainActivityContract
import com.sorelag.testapplication.ui.MainActivityPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
class MainActivityModule(private val mActivity: MainActivity) {

    @ActivityScope
    @Provides
    internal fun provideActivity(): MainActivity {
        return mActivity
    }

    @ActivityScope
    @Provides
    internal fun provideModel(
        repository: MainRepository
    ): MainModel {
        return MainModelImpl(repository)
    }

    @ActivityScope
    @Provides
    internal fun providePresenter(
        model: MainModel,
        connectionWrapper: NetworkConnectionWrapper,
        @Named(RxModule.MAIN) observerScheduler: Scheduler
    ): MainActivityContract.Presenter {
        return MainActivityPresenter(
            model,
            connectionWrapper,
            observerScheduler
        )
    }
}
