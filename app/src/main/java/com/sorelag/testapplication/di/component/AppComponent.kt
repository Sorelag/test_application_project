package com.sorelag.testapplication.di.component

import com.sorelag.testapplication.application.MainApplication
import com.sorelag.testapplication.di.CoreComponent
import com.sorelag.testapplication.di.module.AppModule
import com.sorelag.testapplication.di.module.main.MainActivityComponent
import com.sorelag.testapplication.di.scopes.FeatureScope
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule

@FeatureScope
@Component(
    modules = [AppModule::class, AndroidInjectionModule::class, AndroidSupportInjectionModule::class],
    dependencies = [CoreComponent::class]
)
interface AppComponent {

    fun inject(app: MainApplication)

    fun mainActivityComponent(): MainActivityComponent.Builder

    @Component.Builder
    interface Builder {

        fun appModule(appModule: AppModule): Builder

        fun coreComponent(coreComponent: CoreComponent): Builder

        fun build(): AppComponent
    }
}