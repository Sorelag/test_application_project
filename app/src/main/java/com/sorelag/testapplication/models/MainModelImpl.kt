package com.sorelag.testapplication.models

import com.sorelag.testapplication.data.entities.Rates
import com.sorelag.testapplication.data.mappers.RatesSimpleMapper
import com.sorelag.testapplication.data.repository.MainRepository
import com.sorelag.testapplication.ui.viewModels.RatesViewModel
import io.reactivex.Observable
import javax.inject.Inject

class MainModelImpl @Inject constructor(
    private val mRepository: MainRepository
) : MainModel {

    override fun getData(value: String): Observable<RatesViewModel> {
        return mRepository.getData(value)
            .map(this::convertDataToViewModel)
    }

    private fun convertDataToViewModel(rates: Rates): RatesViewModel? {
        return RatesSimpleMapper().transform(rates)
    }
}