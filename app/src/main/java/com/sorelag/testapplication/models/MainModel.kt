package com.sorelag.testapplication.models

import com.sorelag.testapplication.ui.viewModels.RatesViewModel
import io.reactivex.Observable

interface MainModel {

    fun getData(value: String): Observable<RatesViewModel>
}