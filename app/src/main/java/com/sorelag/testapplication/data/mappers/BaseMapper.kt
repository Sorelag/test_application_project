package com.sorelag.testapplication.data.mappers

import java.util.*

abstract class BaseMapper<From, To> {

    abstract fun transform(from: From): To?

    fun transform(fromList: List<From>): List<To> {
        val toList = ArrayList<To>(fromList.size)
        for (from in fromList) {
            val to = transform(from)
            if (to != null) {
                toList.add(to)
            }
        }
        return toList
    }
}