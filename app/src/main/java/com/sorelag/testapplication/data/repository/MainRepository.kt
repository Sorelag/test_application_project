package com.sorelag.testapplication.data.repository

import com.sorelag.testapplication.data.entities.Rates
import io.reactivex.Observable

interface MainRepository {

    fun getData(value: String): Observable<Rates>
}