package com.sorelag.testapplication.data.entities

/**
{
    "base": "EUR",
    "date": "2018-09-06",
    "rates": {[CountryRates]]}
}
 */
class Rates(
    val base: String,
    val date: String,
    val rates: CountryRates
)