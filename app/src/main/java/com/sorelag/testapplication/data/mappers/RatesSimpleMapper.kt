package com.sorelag.testapplication.data.mappers

import com.sorelag.testapplication.data.entities.RateValues
import com.sorelag.testapplication.data.entities.Rates
import com.sorelag.testapplication.ui.viewModels.RatesViewModel

// TODO: Find better way to map rates
// TODO: Add unit test
class RatesSimpleMapper : BaseMapper<Rates, RatesViewModel>() {

    override fun transform(from: Rates): RatesViewModel? {

        val viewModel = RatesViewModel()
        val dataList: MutableList<Pair<String, Double>> = mutableListOf()

        dataList.add(Pair(from.base, 0.0))

        if (from.base != RateValues.USD) {
            dataList.add(Pair(RateValues.USD, from.rates.USD))
        }
        if (from.base != RateValues.EUR) {
            dataList.add(Pair(RateValues.EUR, from.rates.EUR))
        }
        if (from.base != RateValues.JPY) {
            dataList.add(Pair(RateValues.JPY, from.rates.JPY))
        }
        if (from.base != RateValues.CAD) {
            dataList.add(Pair(RateValues.CAD, from.rates.CAD))
        }

        viewModel.data = dataList
        return viewModel
    }
}