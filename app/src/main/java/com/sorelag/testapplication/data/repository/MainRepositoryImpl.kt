package com.sorelag.testapplication.data.repository

import com.sorelag.testapplication.application.network.NetworkApi
import com.sorelag.testapplication.data.entities.Rates
import io.reactivex.Observable
import io.reactivex.Scheduler
import java.util.concurrent.TimeUnit

class MainRepositoryImpl(
    private val mApi: NetworkApi,
    private val mNetworkScheduler: Scheduler
) :
    MainRepository {

    companion object {
        private const val DELAY: Long = 1000
    }

    override fun getData(value: String): Observable<Rates> {
        return mApi.getData(value)
            .subscribeOn(mNetworkScheduler)
            .delay(DELAY, TimeUnit.MILLISECONDS)
            .repeat()
    }
}