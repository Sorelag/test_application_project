package com.sorelag.testapplication.data.entities

import androidx.annotation.StringDef

@Retention(AnnotationRetention.SOURCE)
@StringDef(
    RateValues.EUR,
    RateValues.USD,
    RateValues.JPY,
    RateValues.CAD
)
annotation class RateValues {
    companion object {
        const val EUR = "EUR"
        const val USD = "USD"
        const val JPY = "JPY"
        const val CAD = "CAD"
    }
}