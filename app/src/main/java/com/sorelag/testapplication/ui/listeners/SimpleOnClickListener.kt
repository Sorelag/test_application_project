package com.sorelag.testapplication.ui.listeners

interface SimpleOnClickListener {
    fun onClick(pos: Int, value: String)
}