package com.sorelag.testapplication.ui

import com.sorelag.testapplication.core.BasePresentationContract
import com.sorelag.testapplication.ui.viewModels.RatesViewModel

interface MainActivityContract {

    interface View : BasePresentationContract.BaseView {

        fun showNoConnection()

        fun showError()

        fun showData(data: RatesViewModel)
    }

    interface Presenter : BasePresentationContract.BasePresenter<View> {

        fun getData()

        fun changeValue(value: String)
    }
}