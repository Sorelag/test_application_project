package com.sorelag.testapplication.ui.adapters

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sorelag.testapplication.R
import com.sorelag.testapplication.ui.listeners.SimpleOnClickListener
import com.sorelag.testapplication.ui.viewModels.RatesViewModel
import kotlinx.android.synthetic.main.item_data.view.*
import java.text.DecimalFormat
import java.text.NumberFormat


internal class MainAdapter(
    val context: Context,
    val listener: SimpleOnClickListener
) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private var data: List<Pair<String, Double>> = emptyList()
    private var value: Int = 0
    val formatter: NumberFormat = DecimalFormat("0.00")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_data, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(data[position], position)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setData(model: RatesViewModel) {
        data = model.data
        notifyDataSetChanged()
    }

    internal inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(data: Pair<String, Double>, pos: Int) {
            itemView.tvName.text = data.first
            if (pos != 0) {
                itemView.etRates.setText(formatter.format(value * data.second).toString())
            } else {
                itemView.etRates.addTextChangedListener(EditTextListener())
            }
            itemView.setOnClickListener { listener.onClick(pos, data.first) }
        }
    }

    private inner class EditTextListener : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            // no op
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            var input: CharSequence
            if (charSequence.isEmpty()) {
                input = "0"
            } else {
                input = charSequence.toString()
            }
            value = Integer.parseInt(input.toString())
        }

        override fun afterTextChanged(editable: Editable) {
            // no op
        }
    }
}