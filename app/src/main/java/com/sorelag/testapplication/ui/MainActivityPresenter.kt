package com.sorelag.testapplication.ui

import android.annotation.SuppressLint
import com.sorelag.testapplication.application.utils.NetworkConnectionWrapper
import com.sorelag.testapplication.core.BasePresenter
import com.sorelag.testapplication.data.entities.RateValues
import com.sorelag.testapplication.models.MainModel
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainActivityPresenter @Inject constructor(
    private val mModel: MainModel,
    private val mConnectionWrapper: NetworkConnectionWrapper,
    private val mObserverScheduler: Scheduler
) : BasePresenter<MainActivityContract.View>(),
    MainActivityContract.Presenter {

    private var value: String = RateValues.EUR

    private var disposable: Disposable? = null

    @SuppressLint("CheckResult")
    override fun getData() {
        if (!mConnectionWrapper.isNetworkAvailable()) {
            view?.showNoConnection()
            return
        }

        disposable =
            mModel.getData(value)
                .subscribeOn(Schedulers.io())
                .observeOn(mObserverScheduler)
                .subscribe({ data ->
                    view?.showData(data)
                }, { throwable ->
                    view?.showError()
                }, {
                }, { disposable ->
                })
    }

    override fun changeValue(value: String) {
        this.value = value
        disposable?.dispose()
        getData()
    }
}
