package com.sorelag.testapplication.ui

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.sorelag.testapplication.R
import com.sorelag.testapplication.core.BaseActivity
import com.sorelag.testapplication.di.component.AppComponent
import com.sorelag.testapplication.di.module.main.MainActivityModule
import com.sorelag.testapplication.ui.adapters.MainAdapter
import com.sorelag.testapplication.ui.listeners.SimpleOnClickListener
import com.sorelag.testapplication.ui.viewModels.RatesViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainActivityContract.View {

    private lateinit var adapter: MainAdapter

    override fun setupDependencies(appComponent: AppComponent) {
        appComponent.mainActivityComponent()
            .module(MainActivityModule(this))
            .build()
            .inject(this)
    }

    @Inject
    internal lateinit var mPresenter: MainActivityContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mPresenter.attachView(this)
        initUi()
    }

    override fun onStart() {
        super.onStart()
        mPresenter.getData()
    }

    private fun initUi() {
        rvRecycler.layoutManager = LinearLayoutManager(this)
        rvRecycler.setHasFixedSize(true)
        rvRecycler.setItemViewCacheSize(10)
        rvRecycler.itemAnimator = null

        adapter = MainAdapter(this, object : SimpleOnClickListener {
            override fun onClick(pos: Int, value: String) {

                mPresenter.changeValue(value)
            }
        })
        rvRecycler.adapter = adapter
    }

    override fun showNoConnection() {
        Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show()
    }

    override fun showError() {
        Toast.makeText(this, "Something wrong", Toast.LENGTH_LONG).show()
    }

    override fun showData(data: RatesViewModel) {
        adapter.setData(data)
    }
}
